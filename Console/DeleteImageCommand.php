<?php

namespace OX\DeleteImages\Console;

use Exception;
use Magento\Framework\App\Area;
use Magento\Framework\App\State;
use OX\DeleteImages\Model\DeleteImages;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class DeleteImageCommand extends Command
{
    protected $deleteImages;

    protected $state;

    protected $logger;

    public function __construct(
        LoggerInterface $logger,
        State $state,
        DeleteImages $deleteImages,
        string $name = null
    ) {
        $this->logger = $logger;
        $this->deleteImages = $deleteImages;
        $this->state = $state;
        parent::__construct($name);
    }

    protected function configure()
    {
        $this->setName('catalog:image:delete-unwanted')
            ->setDescription('To delete the product images based on the attribute value');

        parent::configure();
    }

    /**
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     * @param InputInterface $input
     * @param OutputInterface $output
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        try {
            $this->state->setAreaCode(Area::AREA_FRONTEND);

            $outOfStockProducts = $this->deleteImages->getOutOfStockProducts();
            if ($outOfStockProducts) {
                $this->deleteImages->deleteImages($outOfStockProducts);
            }

            $compositeOutOfStock = $this->deleteImages->getOutOfStockProducts(true);
            if ($compositeOutOfStock) {
                $this->deleteImages->deleteImages($compositeOutOfStock);
            }

            if ($outOfStockProducts || $compositeOutOfStock) {
                $output->writeln('<info>Processed successfully</info>');
            }

            if (!$outOfStockProducts && !$compositeOutOfStock) {
                $output->writeln('<info>Nothing selected to process</info>');
            }
        } catch (Exception $e) {
            $output->writeln('<error>Some exception occurred, please check the log</error>');
            $this->logger->debug($e->getMessage());
        }
    }
}
