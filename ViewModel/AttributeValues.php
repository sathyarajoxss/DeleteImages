<?php

namespace OX\DeleteImages\ViewModel;

use Magento\Catalog\Model\Product;
use Magento\Catalog\Model\ResourceModel\Product as ProductResource;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\View\Element\Block\ArgumentInterface;
use OX\DeleteImages\Helper\Data;

class AttributeValues implements ArgumentInterface
{
    protected $attributeValue;
    protected $productResource;

    public function __construct(
        Data $attributeValue,
        ProductResource $productResource
    ) {
        $this->attributeValue = $attributeValue;
        $this->productResource = $productResource;
    }

    public function getConfigAttributeValues()
    {
        return $this->attributeValue->getAttributeValue();
    }

    /**
     * @param $product Product
     * @return mixed|null
     * @throws LocalizedException
     */
    public function isOutOfProduction($product)
    {
        $customAttribute = null;
        $attributeCode = $this->attributeValue->getAttributeCode();
        $attributeValues = str_getcsv($this->attributeValue->getAttributeValue(), ",", "'");
        if ($product->getCustomAttribute($attributeCode)) {
            $customAttribute = $this->productResource->getAttribute($attributeCode)->getFrontend()->getValue($product);
            if ($customAttribute && in_array($customAttribute, $attributeValues)) {
                return $customAttribute;
            }
        }
        return null;
    }
}
