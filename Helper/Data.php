<?php

namespace OX\DeleteImages\Helper;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Store\Model\ScopeInterface;

class Data extends AbstractHelper
{
    const XML_PATH_ATTRIBUTE_VALUE = 'catalog/deleteImages/attributeValue';
    const XML_PATH_ATTRIBUTE_CODE = 'catalog/deleteImages/attributeDropdown';

    public function getAttributeValue($storeId = null)
    {
        return $this->getConfigValue(self::XML_PATH_ATTRIBUTE_VALUE, $storeId);
    }
    public function getAttributeCode($storeId = null)
    {
        return $this->getConfigValue(self::XML_PATH_ATTRIBUTE_CODE, $storeId);
    }

    public function getConfigValue($field, $storeId = null)
    {
        return $this->scopeConfig->getValue(
            $field,
            ScopeInterface::SCOPE_STORE,
            $storeId
        );
    }
}
