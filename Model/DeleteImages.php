<?php

namespace OX\DeleteImages\Model;

use Exception;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Catalog\Model\Product\Visibility;
use Magento\Catalog\Model\ResourceModel\Product\CollectionFactory;
use Magento\Eav\Model\Config;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\Exception\FileSystemException;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Filesystem;
use Magento\Framework\Filesystem\Driver\File;
use OX\DeleteImages\Helper\Data;
use Psr\Log\LoggerInterface;

class DeleteImages
{
    protected $attributeValue;
    protected $productCollection;
    protected $productRepository;
    protected $file;
    protected $filesystem;
    protected $logger;
    protected $config;

    public function __construct(
        Config $config,
        LoggerInterface $logger,
        Filesystem $filesystem,
        File $file,
        ProductRepositoryInterface $productRepository,
        CollectionFactory $productCollection,
        Data $attributeValue
    ) {
        $this->logger = $logger;
        $this->file = $file;
        $this->filesystem = $filesystem;
        $this->productRepository = $productRepository;
        $this->attributeValue = $attributeValue;
        $this->productCollection = $productCollection;
        $this->config = $config;
    }

    /**
     * @param null $onlyCompositeProduct
     * @return array
     * @throws LocalizedException
     */
    public function getOutOfStockProducts($onlyCompositeProduct = null)
    {
        $attributeCode = $this->attributeValue->getAttributeCode();
        $attributeValues = $this->getAttributeValues();

        $productCollection = $this->productCollection->create();
        if ($onlyCompositeProduct) {
            return $productCollection->addFieldToSelect('entity_id')
                ->addFieldToFilter($attributeCode, ['in' => $attributeValues])
                ->addFieldToFilter('type_id', ['nin' => ['simple', 'virtual', 'downloadable']])
                ->joinField(
                    'stock_item',
                    'cataloginventory_stock_item',
                    'is_in_stock',
                    'product_id=entity_id',
                    'is_in_stock=0'
                )
                ->getAllIds();
        }

        return $productCollection->addFieldToSelect('entity_id')
            ->addFieldToFilter($attributeCode, ['in' => $attributeValues])
            ->addFieldToFilter('type_id', ['in' => ['simple', 'virtual', 'downloadable']])
            ->joinField(
                'stock_item',
                'cataloginventory_stock_item',
                'is_in_stock',
                'product_id=entity_id',
                'is_in_stock=0'
            )
            ->getAllIds();
    }

    /**
     * @throws LocalizedException
     */
    public function getAttributeValues()
    {
        $attributeValues = [];
        $attributeCode = $this->attributeValue->getAttributeCode();
        $attributeLabels = str_getcsv($this->attributeValue->getAttributeValue(), ",", "'");
        $eavConfig = $this->config->getAttribute('catalog_product', $attributeCode);
        $attributeInputType = $eavConfig->getFrontendInput();

        if ($attributeInputType == 'select' || $attributeInputType == 'boolean') {
            foreach ($attributeLabels as $attributeLabel) {
                $attributeValues[] = $eavConfig->getSource()->getOptionId($attributeLabel);
            }
            return array_filter($attributeValues);
        }

        if ($attributeInputType == 'multiselect') {
            $multiAttributes = [];
            foreach ($attributeLabels as $attributeLabel) {
                $isMultiSelect = strpos($attributeLabel, ',');
                if ($isMultiSelect) {
                    $spitedLabels = explode(',', $attributeLabel);
                    foreach ($spitedLabels as $spitedLabel) {
                        $spitedLabel = trim($spitedLabel);
                        $multiAttributes[] = $eavConfig->getSource()->getOptionId($spitedLabel);
                    }
                    $attributeValues[] = implode(',', $multiAttributes);
                }
                if (!$isMultiSelect) {
                    $attributeValues[] = $eavConfig->getSource()->getOptionId($attributeLabel);
                }
            }
            return array_filter($attributeValues);
        }
        return $attributeLabels;
    }

    public function deleteImages($outOfStockProducts)
    {
        try {
            foreach ($outOfStockProducts as $key => $outOfStockProduct) {
                $message = '';
                $product = $this->productRepository->getById($outOfStockProduct, false, 0);
                if ($product->getVisibility() != Visibility::VISIBILITY_NOT_VISIBLE &&
                    $product->getVisibility() != Visibility::VISIBILITY_IN_SEARCH) {
                    $product->setVisibility(Visibility::VISIBILITY_IN_SEARCH);
                    $message = "Visibility is modified";
                }
                $existingMedia = $this->deleteGalleryMedia($product->getMediaGalleryEntries());
                if (count($product->getMediaGalleryEntries()) > 1) {
                    if ($message) {
                        $message .= " | ";
                    }
                    $message .= "Images are deleted ";
                }
                if (!$message) {
                    $message = "No changes";
                }
                // @codingStandardsIgnoreStart
                echo "SKU" . " - " . $product->getSku() . " | " . $message . "\n";
                // @codingStandardsIgnoreEnd
                $product->setMediaGalleryEntries($existingMedia);
                $this->productRepository->save($product);
            }
        } catch (Exception $exception) {
            $this->logger->debug($exception->getMessage());
        }
    }

    public function deleteGalleryMedia($existingMedia)
    {
        $mediaRootDir = $this->filesystem->getDirectoryRead(DirectoryList::MEDIA)->getAbsolutePath();
        foreach ($existingMedia as $key => $entry) {
            if ($key > 0) {
                $mediaRootDir .= 'catalog/product' . $entry->getFile();
                try {
                    if ($this->file->isExists($mediaRootDir)) {
                        $this->file->deleteFile($mediaRootDir);
                    }
                    unset($existingMedia[$key]);
                } catch (FileSystemException $exception) {
                    $this->logger->debug($exception->getMessage());
                }
            }
        }
        return $existingMedia;
    }
}
