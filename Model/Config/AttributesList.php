<?php

namespace OX\DeleteImages\Model\Config;

use Magento\Catalog\Model\Product as ProductEntityType;
use Magento\Eav\Api\Data\AttributeInterface;
use Magento\Eav\Model\Config;
use Magento\Eav\Model\ResourceModel\Entity\Attribute\CollectionFactory as AttributeCollectionFactory;
use Magento\Framework\Data\OptionSourceInterface;
use Magento\Framework\Exception\LocalizedException;

class AttributesList implements OptionSourceInterface
{
    const KEY_IS_USER_DEFINED = 'is_user_defined';
    private $attributeFactory;
    private $eavConfig;

    /**
     * ProductAttribute constructor.
     *
     * @param AttributeCollectionFactory $attributeFactory
     * @param Config $eavConfig
     */
    public function __construct(
        AttributeCollectionFactory $attributeFactory,
        Config $eavConfig
    ) {
        $this->eavConfig = $eavConfig;
        $this->attributeFactory = $attributeFactory;
    }

    /**
     * @return array
     * @throws LocalizedException
     */
    public function toOptionArray(): array
    {
        $collection = $this->attributeFactory->create();
        $collection->addFieldToFilter(self::KEY_IS_USER_DEFINED, 1)
            ->addFieldToFilter('entity_type_id', $this->eavConfig->getEntityType(ProductEntityType::ENTITY)
                ->getEntityTypeId());
        $attributeCodes = [];
        foreach ($collection as $attributes) {
            $attributeCodes[] = [
                'value' => $attributes->getData(AttributeInterface::ATTRIBUTE_CODE),
                'label' => $attributes->getData(AttributeInterface::FRONTEND_LABEL)
            ];
        }
        return $attributeCodes;
    }
}
